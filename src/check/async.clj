(ns check.async
  (:require [clojure.spec.alpha :as s]
            [clojure.string :as str]
            [check.core :as core]
            [clojure.test :as test]
            [clojure.core.async :as async]
            [promesa.core :as p]
            [clojure.walk :as walk]
            [net.cgrand.macrovich :as macros]
            [clojure.core.async.impl.protocols :as proto])
  (:import [clojure.core.async.impl.protocols ReadPort]))

(def ^:dynamic timeout 3000)
(defn- get-from-channel! [chan]
  `(cond
     (promesa.core/promise? ~chan) (async/go @~chan)
     (instance? ReadPort ~chan) ~chan
     :else (async/go ~chan)))

(defmacro await! [chan]
  (macros/case
   :cljs `(to-promise ~chan)
   :clj `(async/<!! ~(get-from-channel! chan))))

(defmacro testing
  "Same as clojure.test/testing, but honors async tests - that is, if a failure occurs
inside this `testing` block, the description will be shown on the console"
  [description & body]
  (macros/case
   :clj `(test/testing ~description ~@body)
   :cljs `(promesa.core/finally
           (promesa.core/do!
            (test/update-current-env! [:testing-contexts] conj ~description)
            ~@body)
           (fn [ & _#] (test/update-current-env! [:testing-contexts] rest)))))

(defmacro let-testing
  "Equivalent to:

```
(testing <description>
  (p/let <bindings>
    (p/do!
      <body>)))
```

Or, more specifically, allows to have a `testing` block that honors async bindings
(it'll await all promises) and async bodies (it'll await all commands that return
promises)
"
  [description bindings & body]
  `(testing ~description
     (promesa.core/let ~bindings
       (p/do! ~@body))))

(defn async-test* [description timeout teardown-delay go-thread]
  (test/testing description
    (let [[res] (async/alts!! [go-thread
                               (async/thread (async/<!! (async/timeout timeout)) ::timeout)]
                              :priority true)]
      (case res
        ::ok @teardown-delay
        ::timeout (do
                    @teardown-delay
                    (throw (ex-info "Async error - not finalized" {:timeout timeout})))
        (do
          @teardown-delay
          (throw res))))))

(s/def ::description string?)
(s/def ::teardown fn?)
(s/def ::timeout int?)
(s/def ::params map? #_(s/keys :opt-un [::timeout ::teardown]))
(s/def ::body any?)
(s/def ::full-params
  (s/cat :description ::description
         :params (s/? ::params)
         :body (s/* ::body)))

(defmacro async-test
  "Starts an \"async test\". Basically, wraps everything around `promesa.core/do!` to
await for promises if they appear, and awaits for 2s to see if the test will pass -
otherwise, the test fails with a timeout exception. It also will capture errors from
the promises, re-throw instead of not ending the test.

This macro is **specially useful** on ClojureScript tests, where async tests are
really hard to get it right. If you intend to use async tests, also use the `testing`
macro in this same namespace - it'll also await for promises and play nice with this
code.

`async-test` expects a test description, and the second argument can be a map
containing `:timeout` and `:teardown`. `:timeout` is a time, in miliseconds, that
the test will await until it fails, and teardown is a **code** (NOT a function)
that will run when the test ends - both when it completes, fails, or timeouts. If
the second parameter is not a map, it'l be interpreted as the test body.

Examples:

```
(ns some-test
  (:require [check.async :as async]
            [clojure.test :as t]))

; A simple async test
(t/deftest sum
  (async/async-test \"some async function\"
    (async/testing \"will sum 2 numbers\"
      ; if sum returns a promise, it'll await for it to resolve
      (async/check (sum 1 2) => 3))))

; A complete example
(def conn (atom nil))
(t/deftest db-query
  (async/async-test \"searches for people\"
    {:timeout 4000, :teardown (db/close! @conn)}

    ; It'll await for this promise to resolve before continuing
    (.then (db/connect!) #(reset! conn %))
    ; It'll also await for this code
    (async/check (db/query! @conn {:person-id 1})
                 => [{:person-id 1, :name \"Person Name\"}])))
```

See also: `check.async/check`, `check.async/testing`
"
  [ & params]
  (let [{:keys [description params body]} (s/conform ::full-params params)
        timeout (:timeout params 2000)
        teardown (:teardown params nil)
        metadata (meta (first body))]
    (macros/case
     :clj `(async-test* ~description ~timeout
             (delay ~teardown)
             (async/thread (try ~@body ::ok (catch Throwable t# t#))))
     :cljs `(async-test* ~timeout (with-meta (fn [] ~teardown) ~metadata)
              (testing ~description ~@body)))))
(s/fdef async-test :args (s/cat :params ::full-params))

(defmacro check
  "Checks for an expression, using a matcher (or arrow), in an async context.

`check` have two APIs: one that can be used as `is`, but only works for matchers or
arrows, and other that is similar to midje's `fact`. To us the first version, you can
use:

```clojure
; This will use the `nubank/matcher-combinators` match? to check equality
(check (=> <expected> <actual>))

; This will use the `expectations` library to check equality
(check (=expect=> <expected> <actual>))

; Or you can use the midje-like assertions
(check <actual> => <expected>)
```

This version of `check` will \"await\" for promises and also return promises. It also
is not vulnerable to problems with matcher-combinators and clojure.test with async
code either.

See also: `check.core/check`, `check.async/async-test`"
  ([sub-expression]
   (let [[arrow right left] sub-expression]
     `(check ~left ~arrow ~right)))
  ([left arrow right]
   (let [cljs? (macros/case :cljs true :clj false)
         lft (gensym "left-")
         rgt (gensym "right-")
         params (or (meta left) (meta arrow))]
     (macros/case
      :clj `(let [~lft (await! ~left)
                  ~rgt (await! ~right)]
              (test/do-report (-> ~params
                                  (merge ~(core/assert-arrow cljs? lft arrow rgt))
                                  (assoc :expected (quote ~right)))))
      :cljs `(promesa.core/let [~lft (await! ~left) ~rgt (await! ~right)]
               (test/do-report (-> ~params
                                   (merge ~(core/assert-arrow cljs? lft arrow rgt))
                                   (assoc :expected (quote ~right))))
               :done)))))
