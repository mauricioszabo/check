(ns check.mocks
  (:require [clojure.spec.alpha :as s]
            [check.async :as async]
            [promesa.core :as p]
            [net.cgrand.macrovich :as macros]
            [clojure.string :as str]))

(s/def ::arrow '#{=> =streams=>})
(s/def ::template (s/cat :fn symbol? :args (s/* any?)))
(s/def ::mocks (s/cat
                :mocks (s/+ (s/cat :template (s/spec ::template) :arrow ::arrow :return any?))
                :arrow '#{--- ===}
                :body (s/* any?)))

(defn- normalize-return [{:keys [arrow fn args return]}]
  (case arrow
    => {:return return}
    =streams=> (let [s (gensym "stream-")]
                 {:let-fn `[~s (atom ~return)]
                  :fn `(fn []
                         (when (empty? @~s)
                           (throw (ex-info "No more values to stream on mock"
                                           {:function '~fn
                                            :args ~args})))
                         (let [ret# (first @~s)]
                           (swap! ~s rest)
                           ret#))})))

(defn- normalize-mocking-params [mockings]
  (->> mockings
       (map (fn [{:keys [template return arrow]}]
              [(:fn template) (assoc template :arrow arrow :return return)]))
       (group-by first)
       (map (fn [[k v]]
              [k (->> v
                      (map (fn [[_ v]] [(:args v) (normalize-return v)]))
                      (into {}))]))))

(defn- to-function [[fn-name args+return]]
  (let [all-lets (->> args+return
                      (map (comp :let-fn second))
                      (filter identity)
                      (mapcat identity))
        fun (name fn-name)]

    [fn-name
     `(let [~@all-lets]
        (fn ~(symbol fun) [ & old-args#]
          (if-let [return# (get ~args+return old-args#)]
            (let [{:keys [~'fn ~'return]} return#]
              (cond
                ~'fn (~'fn)
                ~'return ~'return))
            (throw (ex-info "No mocked calls for this fn/args"
                            {:function '~fn-name
                             :expected-args (keys ~args+return)
                             :actual-args old-args#})))))]))

(defn bind! [fn-name mock]
  `(set! ~fn-name ~mock))

(defn- prepare-lets-and-mocks [acc [fun impl]]
  (let [old-fn-name (gensym (name fun))]
    (-> acc
        (update :lets conj old-fn-name fun)
        (update :mocks conj {:fn-name fun
                             :mock impl
                             :revert-name old-fn-name}))))

(defn- cljs [pairs body]
  (let [mockings (reduce prepare-lets-and-mocks {:lets [] :mocks []} pairs)
        de-mock (map (fn [{:keys [fn-name revert-name]}] (bind! fn-name revert-name))
                     (:mocks mockings))]
    `(let [~@(:lets mockings)
           async# (atom false)]
         (try
           ~@(map (fn [{:keys [fn-name mock]}] (bind! fn-name mock))
                  (:mocks mockings))
           (let [res# (do ~@body)]
             (reset! async# (p/promise? res#))
             (when @async# (p/finally res# (fn [] ~@de-mock)))
             res#)
           (finally
             (when-not @async# ~@de-mock))))))

(defn- clj [mocking-pair body]
 (let [mockings (mapcat identity mocking-pair)]
   `(with-redefs [~@mockings]
      (let [res# (do ~@body)]
        (when (or (instance? java.util.concurrent.Future res#)
                  (instance? clojure.lang.IDeref res#))
          @res#)
        res#))))

(defmacro with-mocks [mocks & body]
  "Almost the same as `with-redefs-fn`, but also works with async (promise-based) tests.
It's a macro because ClojureScript is limited on this regard

```
(with-mocks {+ (fn [a b] (+ a b 1))}
  (+ 1 2) => 4
  (+ 10 2) => 13
```"
  (macros/case
   :cljs (cljs mocks body)
   :clj (clj mocks body)))

(defmacro mocking
  "Almost the same as `with-redefs`, but makes it easy to return different data for every
subsequent call, and also works with async (promise-based) tests. Expects a function
with their specific inputs, and arrow, and the outputs, then a separator `---`, and
the code to which the mock will be applied

```
(mocking
  (+ 1 2) => 4
  (+ 3 4) =streams=> [5 6 7]
  ---
  (+ 1 2) ; Will return 4
  (+ 3 4) ; Will return 5
  (+ 3 4) ; Will return 6
  (+ 3 4) ; Will return 7
  (+ 3 4)) ; Will crash with an exception
```"
  [ & args]
  (s/assert* ::mocks args)
  (let [{:keys [mocks body]} (s/conform ::mocks args)
        mockings (->> mocks
                      normalize-mocking-params
                      (map to-function)
                      doall)]
    `(with-mocks ~mockings ~@body)))
