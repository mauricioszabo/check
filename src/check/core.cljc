(ns check.core
  #?(:cljs (:require-macros [check.core]))
  (:require [clojure.string :as str]
            [expectations :refer [compare-expr ->failure-message in]]
            [clojure.test :refer [do-report]]
            #?(:cljs [cljs.test])
            [net.cgrand.macrovich :as macros]
            [matcher-combinators.core :as c]
            [matcher-combinators.printer :as p]
            [matcher-combinators.test]))

(declare => =expect=> =includes=>)

(defmulti assert-arrow (fn [cljs? left arrow right] arrow))

(defmethod assert-arrow '=> [_ left _ right]
  `(let [lft# ~left
         rgt# ~right
         qleft# (quote ~left)
         qright# (quote ~right)
         result# (c/match rgt# lft#)
         msg# (->> result# :matcher-combinators.result/value p/as-string
                   (str (pr-str lft#) "\n\nMismatch:\n"))
         pass?# (= :match (:matcher-combinators.result/type result#))]
     {:type (if pass?# :pass :fail)
      :expected qright#
      :actual (symbol msg#)}))

(defmethod assert-arrow '=expect=> [_ left _ right]
  `(let [lft# ~left
         rgt# ~right
         qleft# (quote ~left)
         qright# (quote ~right)
         result# (compare-expr rgt# lft# qright# qleft#)
         unformatted-msg# (expectations/->failure-message result#)
         msg# (str/replace unformatted-msg# #"^.*?\n" (str qleft# " => " qright#))]
     {:type (:type result#)
      :message msg#
      :expected qright#
      :actual qleft#}))

(defmethod assert-arrow '=includes=> [_ left _ right]
  `(check (in ~left) ~'=expect=> ~right))

#?(:cljs
   (defn normalize-error [error]
     (let [stacktrace (or (.-stack error)
                          (.-stack (js/Error.)))]
       (symbol (str (pr-str error) "\n" stacktrace)))))

(defmacro check
  "Checks for an expression, using a matcher (or arrow).

`check` have two APIs: one that can be used as `is`, but only works for matchers or
arrows, and other that is similar to midje's `fact`. To us the first version, you can
use:

```clojure
; This will use the `nubank/matcher-combinators` match? to check equality
(check (=> <expected> <actual>))

; This will use the `expectations` library to check equality
(check (=expect=> <expected> <actual>))

; Or you can use the midje-like assertions
(check <actual> => <expected>)
```

If you want to use it to check for Javascript's promises, see check.async/check. Its
usage is the same, but it'll always \"await\" for promises and also return promises."
  ([sub-expression]
   (let [[arrow right left] sub-expression]
     `(check ~left ~arrow ~right)))
  ([left arrow right]
   (let [params (or (meta left) (meta arrow))]
     (macros/case
      :clj
      `(try
         (let [report# ~(assert-arrow false left arrow right)]
           (do-report (if (map? report#) (merge ~params report#) report#)))
         (catch java.lang.Throwable t#
           (do-report (assoc ~params
                             :type :error
                             :message (str "Expected " (quote ~left) " " (quote ~arrow) " " (quote ~right))
                             :expected ~right
                             :actual t#))))
      :cljs
      `(try
         (let [report# ~(assert-arrow true left arrow right)]
           (do-report (if (map? report#) (merge ~params report#) report#)))
         (catch :default t#
           (do-report (assoc ~params
                             :type :error
                             :message (str "Expected " (quote ~left) " " (quote ~arrow) " " (quote ~right))
                             :expected ~right
                             :actual (normalize-error t#)))))))))

(defn- prepare-symbol [left ex]
  `(let [qleft# (quote ~left)]
     (try
       {:type :fail
        :message (str "Expected " qleft# " to throw error " (quote ~ex))
        :expected (quote ~ex)
        :actual ~left}
       (catch ~ex t#
         {:type :pass}))))

(defn- prepare-coll [left right]
  (let [[ex fun] right]
    `(let [qleft# (quote ~left)
           qright# (quote ~right)]
       (try
         {:type :fail
          :message (str "Expected " qleft# " to throw error " (quote ~ex))
          :expected qright#
          :actual ~left}
         (catch ~ex t#
           (when ~fun
             (~fun t#)))))))

(defmethod assert-arrow '=throws=> [cljs? left _ right]
  (assert (or (and (coll? right) (-> right first symbol?))
              (symbol? right)))
  (if (symbol? right)
    (prepare-symbol left right)
    (prepare-coll left right)))

(def custom-matchers (atom {}))
(defmethod assert-arrow :default [cljs? left matcher right]
  `(if-let [matcher# (get @custom-matchers '~matcher)]
     (let [lft# ~left
           rgt# ~right
           res# (matcher# rgt# lft#)]
       (if (:pass? res#)
         {:type :pass
          :message '(~'check 'lft# => 'rgt#)}
         {:type :fail
          :expected '~right
          :actual (symbol (str (:failure-message res#)))}))
     {:type :error
      :expected ~right
      :actual (str "Matcher " '~matcher " is not implemented")}))

(defmacro defmatcher [name args & body]
  `(do
     (declare ~name)
     (swap! custom-matchers assoc '~name (fn ~args ~@body))))
