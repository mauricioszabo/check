(ns check.async
  (:require-macros [check.async])
  (:require [clojure.string :as str]
            [check.core :as core]
            [clojure.test :as test]
            [cljs.test]
            [clojure.core.async :as async]
            [promesa.core :as p]
            [cljs.core.async.impl.protocols :as proto]
            [promesa.protocols :as promesa-proto]))

(defn to-promise [promise-or-chan]
  (if (satisfies? proto/ReadPort promise-or-chan)
    (let [p (p/deferred)]
      (async/go
        (let [res (async/<! promise-or-chan)]
          (p/resolve! p res)))
      p)
    promise-or-chan))

(defrecord AsyncTest [prom]
  cljs.test/IAsyncTest
  cljs.core/IFn
  (-invoke [_ done] (p/finally prom (fn [ & _] (done))))
  promesa-proto/IPromise
  (-fmap [_ f] (promesa-proto/-fmap prom f))
  (-fmap  [_ f executor] (promesa-proto/-fmap prom f executor))

  (-merr [_ f] (promesa-proto/-merr prom f))
  (-merr [_ f executor] (promesa-proto/-merr prom f executor))

  (-mcat [_ f] (promesa-proto/-mcat prom f))
  (-mcat [_ f executor] (promesa-proto/-mcat prom f executor))

  (-hmap [_ f] (promesa-proto/-hmap prom f))
  (-hmap [_ f executor] (promesa-proto/-hmap prom f executor))

  (-fnly [_ f] (promesa-proto/-fnly prom f))
  (-fnly [_ f executor] (promesa-proto/-fnly prom f executor))

  (-then [_ f] (promesa-proto/-then prom f))
  (-then [_ f executor] (promesa-proto/-then prom f executor))

  promesa-proto/IPromiseFactory
  (-promise [_] prom))

(defn async-test* [timeout teardown prom]
  (let [metadata (meta teardown)
        timeout-error (js/Object.)
        error (js/Object.)
        promise (p/catch prom #(vector error %))
        timeout-prom (p/do!
                      (p/delay timeout)
                      timeout-error)

        full-prom
        (-> (p/race [promise timeout-prom])
            (p/then (fn [res]
                      (cond
                        (and (vector? res) (-> res first (= error)))
                        (do
                          (test/do-report (assoc metadata
                                                 :type :error
                                                 :expected "Not an error"
                                                 :actual (-> res second core/normalize-error))))

                        (= timeout-error res)
                        (test/do-report (assoc metadata
                                               :type :error
                                               :expected "Test to finish"
                                               :actual (core/normalize-error
                                                        (str "Test did ""not finish in "
                                                             timeout "msec")))))

                      (teardown)))
            (p/catch (fn [error] (test/is (not error)) nil)))]
    (->AsyncTest full-prom)))
