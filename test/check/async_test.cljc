(ns check.async-test
   (:require [clojure.test :refer [deftest is] :as t]
             [clojure.core.async :as async :refer [>! timeout go <!]]
             [check.async :refer [async-test await! check testing let-testing]]
             [promesa.core :as p]
             [net.cgrand.macrovich :as macros]
             [clojure.pprint :as pp]
             [clojure.walk :as walk]))

(deftest things-running
  (async-test "when things run correctly"
    (is (= 1 1))))

#?(:clj
   (deftest some-async-test
     (async-test "when there's a async test"
       (let [c (async/promise-chan)]
         (go
          (<! (timeout 200))
          (>! c "ok"))
         (is (= "ok" (await! c)))))))

(defn- async-fun []
  (let [c (async/chan)]
    (go
      (async/<! (async/timeout 100))
      (async/>! c "what\nI want\nto check\nreally\n")
      (async/>! c "something else")
      (async/close! c))
    c))

(deftest multi-check-channel
  (async-test "Resolves with regexp"
    (check (async-fun) => #"to check")))

(deftest checking
  (async-test "when running with check"
    (let [c (async/promise-chan)]
      (go
       (<! (timeout 200))
       (>! c "ok"))
      (check c => "ok")
      (check (=> "ok" c)))))

(deftest promises
  (async-test "adds a checker for promises"
    (check (p/resolved 10) => 10)))

(def teardown (atom :initialized))

(deftest tearing-down
  (async-test "will teardown resources" {:teardown (reset! teardown :done)}
    (let [c (async/promise-chan)]
      (reset! teardown :init)
      (go
       (<! (timeout 100))
       (>! c "ok"))
      (testing "will check"
        (check c => "ok"))))
  #?(:clj (check @teardown => :done)))

(deftest using-testing-let
  (async-test "allow to use testing the same as we use promesa.core/let"
    (let-testing "a sum of elements" [a (p/delay 100 9)
                                      b (p/delay 80 10)]
      (check (+ a b) => 19))

    (let-testing "a delay" [a (p/delay 100 11)
                            b (p/delay 80 12)]
      (p/delay 200)
      (check (+ a b) => 23))))

(macros/case
 :cljs
 (deftest after-teardown
   (check @teardown => :done)))

#_
(deftest promise-rejection
  (async-test "promise error"
    (let [prom (p/rejected (ex-info "a error" {}))]
      (check prom => :done))))
